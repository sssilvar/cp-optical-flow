#!/bin/env python3
__description__ = """ Feature extraction for all the dataset. """
__author__ = 'Santiago Silva'
___year___ = '2019'

import os
from os.path import dirname, realpath, join, normpath, isdir

import pandas as pd

# Define root folder
root = dirname(dirname(realpath(__file__)))

# Define used variables
DATASET_FILE = join(root, 'data', 'dataset.csv')
DATASET_FOLDER = normpath('/disk/Datasets/RIIE')
PROCESSED_FOLDER = normpath('/disk/Datasets/RIIE_processed')

SCRIPT = join(root, 'main.py')


def mkdir(folder_path):
    """Creates a folder if does not exist."""
    try:
        os.makedirs(folder_path, exist_ok=True)
    except FileExistsError:
        pass


def find(filename, folder):
    """Finds a file in a folder by its name."""
    for root_folder, dirs, files in os.walk(folder):
        if filename in files:
            return os.path.join(root_folder, filename)


if __name__ == '__main__':
    # Print some info
    sep = '=' * 15
    print(f'{sep} VIDEO FEATURE EXTRACTOR {sep}')
    print(f'\t- Dataset folder: {DATASET_FOLDER}')
    print(f'\t- Processed folder: {PROCESSED_FOLDER}')

    # Chech that dataset folder exists
    assert isdir(DATASET_FOLDER), f'Dataset folder does not exist ({DATASET_FOLDER}).'

    print('[  INFO  ] Creating Dataset processed folder...')
    mkdir(PROCESSED_FOLDER)

    # Load dataset
    print('[  INFO  ] Loading dataset...')
    dataset = pd.read_csv(DATASET_FILE, index_col='ID')
    print(dataset.head())

    for sid, row in dataset.iterrows():
        sid = f'{sid:003d}'
        print(f'Processing subject {sid}')
        print(f'\t- DX: {row.DX}')

        for task in ['SMOOTH', 'SACCAD']:
            # Process task (SMOOTH/SACCAD)
            video_file = find(row[task], DATASET_FOLDER)
            print(f'\t-{task.capitalize()} task video: {video_file}')
            if not video_file:
                raise FileNotFoundError(f'File {video_file} does not exist.')

            # Create output folder
            out_folder = join(PROCESSED_FOLDER, sid, task)
            mkdir(out_folder)

            # Create command for feature extraction
            command = f'{SCRIPT} --video {video_file} --output {out_folder}'
            exit_code = os.system(command)
            if exit_code:
                raise ValueError('Command exited with non-zero code.')

        print()
