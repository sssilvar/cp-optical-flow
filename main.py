#!/bin/env python3
import os
import sys
import argparse
from os.path import join, isdir, isfile, dirname

from cplab.detectors import EyeDetection
from cplab.optical_flow import FarnebackOpticalFlow


def parse_args():
    parser = argparse.ArgumentParser(description='CP Lab pipeline')
    parser.add_argument('--video', help='Video file to be analyzed', required=True)
    parser.add_argument('--output', help='Output folder (results).', required=True)
    return parser.parse_args()


if __name__ == '__main__':
    # Print Interpreter
    print(f'Python interpreter: {sys.executable}')
    # Parse arguments
    args = parse_args()

    # Load params
    video_file = args.video
    out_folder = args.output

    assert isfile(video_file), f'Video file does not exist: {video_file}'
    if not isdir(out_folder):
        os.mkdir(out_folder)

    # Eye detection
    eye_detector = EyeDetection(
        output_folder=join(out_folder, 'eye_detection'),
        video_file=video_file,
        roi_size=(100, 100)
    )
    eye_detector.detect_eyes()

    # Optical flow calculation
    opt_flow = FarnebackOpticalFlow(
        output_folder=join(out_folder, 'optical_flow'),
        video_file=eye_detector.video_file,
        roi_csv=eye_detector.roi_csv
    )
    opt_flow.compute()
