#!/usr/bin/env bash
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VENV="${CURRENT_DIR}/../venv/"
SCRIPT="${CURRENT_DIR}/../main.py"

# Activate virtual environment
eval "source ${VENV}/bin/activate"

FILES=(
"/home/ssilvari/Documents/temp/Videos/control_3/SMOOTH/GOPR0357.MP4"
"/home/ssilvari/Documents/temp/Videos/cp_3/SMOOTH/GOPR0306.MP4"
#"/home/ssilvari/Documents/temp/Videos/control_2/SACCAD/GOPR0217.MP4"
#"/home/ssilvari/Documents/temp/Videos/cp_2/SACCAD/GOPR0259.MP4"
#"/home/ssilvari/Documents/temp/Videos/control_2/GOPR0216.MP4"
#"/home/ssilvari/Documents/temp/Videos/cp_2/GOPR0258.MP4"
)

for video in ${FILES[@]} ; do
    echo "Processing video: ${video}"
    CMD="${SCRIPT} --video ${video}"
    echo ${CMD}
    eval ${CMD}
done

# Deactivate virtual environment
eval "deactivate"
echo "Done!"