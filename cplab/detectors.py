#!/bin env python3
from os.path import join, dirname, realpath, basename, splitext, isfile

import cv2
import pandas as pd
from .path import mkdir


class EyeDetection:
    def __init__(self, output_folder, video_file, roi_size=(200, 200), codec='mp4v'):
        self.roi_size = roi_size
        self.output_folder = output_folder
        self.video_file = video_file

        self.cwd = dirname(realpath(__file__))
        self.codec = codec
        self.roi_csv = join(self.output_folder, 'eye_rois.csv')

    def detect_eyes(self):
        """
        Detects eyes ROI for a determined video file.
        """
        if isfile(self.roi_csv):
            print(f'[  WARNING  ] Eyes detection file exist {self.roi_csv}')
            return
        # Create output folder
        mkdir(self.output_folder)

        # Create video capture
        video_basename = basename(self.video_file)
        cap = cv2.VideoCapture(self.video_file)

        # Get some info from the video
        fps = cap.get(cv2.CAP_PROP_FPS)  # Frames per second
        size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
                int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        n_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)  # Number of frames in total

        print(f'\t- Video to be processed: {video_basename}')
        print(f'\t- Video size: {size} pixels')
        print(f'\t- Video framerate: {fps} fps')
        print(f'\t- Number of frames: {n_frames}')

        # Define output files
        out_vid = join(self.output_folder, splitext(video_basename)[0] + '_eyes.mkv')
        print(f'\t- Output video: {out_vid}')

        # Setup Haar cascade classifiers for eye detection
        eye_cascade_xml = join(self.cwd, 'haarcascade_eye.xml')
        # eye_cascade_xml = join(self.cwd, 'haarcascade_lefteye_2splits.xml')
        eye_cascade = cv2.CascadeClassifier(eye_cascade_xml)

        # Configure codec
        fourcc = cv2.VideoWriter_fourcc(*self.codec)  # Video codec
        vid_writer = cv2.VideoWriter(out_vid, fourcc, fps, size)

        # Create DataFrame for ROIs
        roi_df = pd.DataFrame()

        # Extract ROI per frame
        print('Procesing video ...')
        print('Processing frame: ', end='')

        # Set a frame counter
        frame_counter = 0

        # While there is still frames to read
        while cap.isOpened():
            # Load frame
            ret, frame = cap.read()
            frame_counter += 1  # Increase by one

            if frame_counter >= n_frames:  # Done if all frames done
                break

            if ret:  # If frame is not corrupted
                # Print frame being processed (each 200 frames or first 10 frames or last 10 frames)
                if frame_counter % 200 == 0 or frame_counter <= 10 or frame_counter > (n_frames - 10):
                    print(f'{frame_counter}..', end='')

                # Convert frame to grayscale
                frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
                # print(frame_gray)

                # Detect eyes in frame_gray
                eyes = eye_cascade.detectMultiScale(frame_gray, scaleFactor=1.1, minNeighbors=10, minSize=self.roi_size)

                # For each eye found
                if len(eyes) == 2:
                    for i, (x, y, w, h) in enumerate(eyes):
                        frame_with_eyes = cv2.rectangle(frame, (x, y), (x + h, y + w), (0, 255, 0), 20)  # ROI
                        series = pd.Series({'x': x, 'y': y, 'w': w, 'h': h, 'eye': i, 'frame': frame_counter})
                        roi_df = roi_df.append(series, ignore_index=True)
                    vid_writer.write(frame_with_eyes)
                else:
                    vid_writer.write(frame)

        # Release file
        print('Releasing input file...')
        cap.release()

        # Finish video file (write to disk)
        print(f'Writing video: {out_vid} ...')
        vid_writer.release()
        cv2.destroyAllWindows()

        # Save as int
        for col in roi_df.columns:
            roi_df[col] = roi_df[col].astype('int')

        # Save ROI coords
        roi_df.to_csv(self.roi_csv)
