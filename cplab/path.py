import os
import shutil


def mkdir(folder_path, rm_existent=False):
    """
    Creates a new folder. Optional removal if exists.
    :param folder_path: Path to be created
    :param rm_existent: Remove folder if exists. Then re-create it.
    :return:
    """
    if rm_existent:
        shutil.rmtree(folder_path)
        os.mkdir(folder_path)
    elif not os.path.isdir(folder_path):
        os.mkdir(folder_path)
