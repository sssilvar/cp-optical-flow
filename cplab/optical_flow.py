#!/bin/env python3
import os
import cv2
import numpy as np
import pandas as pd
from time import sleep

from sklearn import mixture
from scipy.stats import entropy, skewnorm, describe, recipinvgauss, wasserstein_distance
from skimage.segmentation import slic
import matplotlib.pyplot as plt

plt.style.use('ggplot')

from .path import mkdir


class FarnebackOpticalFlow:
    def __init__(self, output_folder, video_file, roi_csv):
        self.output_folder = output_folder
        self.video_file = video_file
        self.roi_csv = roi_csv
        self.codec = 'mp4v'
        self.gmm = mixture.GaussianMixture(n_components=2, covariance_type='full')

    def compute(self):
        """
        Computes Farneback Optical flow for a determined video.
        :return: None. Saves output into output_folder.
        """
        # Create output folder
        mkdir(self.output_folder)

        # Load rois DataFrame
        roi_df = pd.read_csv(self.roi_csv, index_col='frame')
        ext = 'mkv'

        # Get video base name GOPR0XXX
        video_basename = os.path.splitext(os.path.basename(self.video_file))[0]

        # Time series (features) file
        eye_data_file = os.path.join(self.output_folder, video_basename + f'_features_series.csv')
        if os.path.isfile(eye_data_file):
            print(f'[  WARNING  ] Features file {eye_data_file} already exist.')
            return

        # Create DataFrame that will contain the time series
        eye_data = pd.DataFrame()

        for eye_i in range(2):
            # Delete first frame variable if exists
            try:
                del first_frame
            except UnboundLocalError:
                pass

            # Print some info
            s = 15 * '='
            print(f'{s} Processing eye {eye_i} {s}')

            # Generate an output video
            out_vid = os.path.join(self.output_folder, video_basename + f'_of_eye_{eye_i}.{ext}')

            # Load video
            cap = cv2.VideoCapture(self.video_file)

            # Get some info from the video
            fps = cap.get(cv2.CAP_PROP_FPS)  # Frames per second
            size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
                    int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
            n_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)  # Number of frames in total

            print(f'\t- Video to be processed: {video_basename}')
            print(f'\t- Video size: {size} pixels')
            print(f'\t- Video framerate: {fps} fps')
            print(f'\t- Number of frames: {n_frames}')

            # Load ROI
            eye = roi_df.query(f'eye == {eye_i}')
            x = int(eye.loc[:, 'x'].median())
            h = int(eye.loc[:, 'h'].median())
            y = int(eye.loc[:, 'y'].median())
            w = int(eye.loc[:, 'w'].median())
            print(f'Orig: ({x},{y}) | Width: {w} | Height: {h}')

            # Configure codec
            fourcc = cv2.VideoWriter_fourcc(*self.codec)  # Video codec
            vid_writer = cv2.VideoWriter(out_vid, fourcc, fps, (h, w))
            # vid_writer_roi = cv2.VideoWriter(out_vid[:-4] + f'_roi_eye.{ext}', fourcc, fps, (3 * h, w))
            vid_writer_visual = cv2.VideoWriter(out_vid[:-4] + f'_visual.{ext}', fourcc, fps, (3 * h, w))

            print('Processing frames:')
            while cap.isOpened():
                ret, frame = cap.read()
                current_frame = cap.get(cv2.CAP_PROP_POS_FRAMES)
                if current_frame % 150 == 0:
                    print(f'{current_frame}..', end='')

                if ret:
                    roi = frame[y:y + w, x: x + h, :]  # First Y and then X in OpenCV
                    # roi = cv2.fastNlMeansDenoising(roi)
                    saliency = cv2.saliency.StaticSaliencySpectralResidual_create()
                    (success, saliency_map) = saliency.computeSaliency(roi)
                    saliency_map = (saliency_map * 255).astype("uint8")

                    threshold_map = cv2.threshold(saliency_map, 0, 255,
                                              cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

                    img_visual_model = np.hstack([
                        roi,
                        cv2.cvtColor(saliency_map, cv2.COLOR_GRAY2RGB),
                        cv2.cvtColor(threshold_map, cv2.COLOR_GRAY2RGB)
                    ])
                    vid_writer_visual.write(img_visual_model)

                    # cv2.imshow("Saliency map", img_visual_model)
                    # cv2.imshow('ROI', roi)

                if 'first_frame' not in locals() and ret:
                    first_frame = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
                    hsv = np.zeros_like(roi)
                    hsv[..., 1] = 255
                elif ret:
                    next_frame = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
                    flow = cv2.calcOpticalFlowFarneback(first_frame, next_frame, None, 0.5, 3, 15, 3, 5, 1.2, 0)

                    # Extract velocities in X and Y
                    vx, vy = flow[..., 0], flow[..., 1]

                    # Convert to polar coordinates
                    mag, ang = cv2.cartToPolar(vx, vy)
                    hsv[..., 0] = ang * 180 / np.pi / 2
                    hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
                    rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

                    # Weight the results
                    weighted_mag = np.multiply(mag, saliency_map)
                    weighted_ang = np.multiply(ang, saliency_map)

                    weighted_vx = np.multiply(vx, saliency_map)
                    weighted_vy = np.multiply(vy, saliency_map)

                    # Mean inside threshold map
                    mag_th = mag[np.where(threshold_map)]
                    ang_th = ang[np.where(threshold_map)]
                    area_th = threshold_map.sum()

                    # Moments of a skewed Normal
                    # mag_sk_alpha, mag_sk_mean, mag_sk_var = recipinvgauss.fit(weighted_mag.ravel())
                    # ang_sk_alpha, ang_sk_mean, ang_sk_var = recipinvgauss.fit(weighted_ang.ravel())
                    counts_mag, bins_mag = np.histogram(weighted_mag, bins=50)
                    counts_ang, bins_ang = np.histogram(weighted_ang, bins=50)

                    delta_bins_mag = np.diff(counts_mag).mean() / np.diff(bins_mag).mean()
                    delta_bins_ang = np.diff(counts_ang).mean() / np.diff(bins_ang).mean()

                    # Describe weighted distribution
                    wmag_nobs, (wmag_min, wmag_max), wmag_mean, wmag_var, \
                    wmag_skewness, wmag_kurtosis = describe(weighted_mag.ravel())
                    # Same for orientation
                    wang_nobs, (wang_min, wang_max), wang_mean, wang_var, \
                    wang_skewness, wang_kurtosis = describe(weighted_ang.ravel())

                    # Add mean and std to DataFrame
                    frame_data = {
                        'delta_mag': delta_bins_mag,
                        'delta_ang': delta_bins_ang,
                        # 'skew_mag_alpha': mag_sk_alpha,
                        # 'skew_mag_mean':  mag_sk_mean,
                        # 'skew_mag_var':   mag_sk_var,
                        # 'skew_ang_alpha': ang_sk_alpha,
                        # 'skew_ang_mean':  ang_sk_mean,
                        # 'skew_ang_var':   ang_sk_var,
                        'mag_mean':         mag.mean(),
                        'mag_std':          mag.std(),
                        'ang_mean':         ang.mean(),
                        'ang_std':          ang.std(),
                        'mag_th_mean':      mag_th.mean(),
                        'mag_th_median':    np.median(mag_th),
                        'mag_th_std':       mag_th.std(),
                        'ang_th_mean':      ang_th.mean(),
                        'ang_th_median':    np.median(ang_th),
                        'ang_th_std':       ang_th.std(),
                        'mag_th_entropy':   entropy(ang_th.ravel()),
                        'ang_th_entropy':   entropy(ang_th.ravel()),
                        'area_th':          area_th,
                        # Weighted magnitude
                        'wmag_nobs':        wmag_nobs,
                        'wmag_min':         wmag_min,
                        'wmag_max':         wmag_max,
                        'wmag_mean':        wmag_mean,
                        'wmag_var':         wmag_var,
                        'wmag_skewness':    wmag_skewness,
                        'wmag_kurtosis':    wmag_kurtosis,
                        # Weighted orientation
                        'wang_nobs':        wang_nobs,
                        'wang_min':         wang_min,
                        'wang_max':         wang_max,
                        'wang_mean':        wang_mean,
                        'wang_var':         wang_var,
                        'wang_skewness':    wang_skewness,
                        'wang_kurtosis':    wang_kurtosis,
                        'wmag_entropy':     entropy(weighted_mag.ravel()),
                        'wang_entropy':     entropy(weighted_ang.ravel()),
                        # Vx and Vy stats
                        'vx_mean':   vx.mean(),
                        'vx_std':    vx.std(),
                        'vy_mean':   vy.mean(),
                        'vy_std':    vy.std(),
                        # Weighted Vx and Vy
                        'wx_mean':  weighted_vx.mean(),
                        'wx_std':   weighted_vx.std(),
                        'wy_mean':  weighted_vy.mean(),
                        'wy_std':   weighted_vy.std(),
                        # Eye
                        'eye': eye_i,
                        'x': x,
                        'y': y,
                        'h': h,
                        'w': w,
                        'area_roi': w * h
                    }
                    frame_features = pd.Series(data=frame_data, name=current_frame)
                    eye_data = eye_data.append(frame_features)

                    # Superpixels
                    # sp_mag = slic(mag.astype(np.float), n_segments=160, enforce_connectivity=False)
                    # sp_ang = slic(ang.astype(np.float), n_segments=160, enforce_connectivity=False)

                    # if current_frame % 4 == 0:
                    #     counts_mag, bins_mag = np.histogram(weighted_mag, bins=50)
                    #     counts_ang, bins_ang = np.histogram(weighted_ang, bins=50)
                    #
                    #     # Fit data
                    #     # self.gmm.fit(mag)
                    #     # print(self.gmm.means_)
                    #
                    #     fig, ax = plt.subplots(ncols=2, sharey=True)
                    #     ax[0].hist(bins_mag[:-1], bins_mag, weights=counts_mag, alpha=0.6)
                    #     ax[1].hist(bins_ang[:-1], bins_ang, weights=counts_ang, alpha=0.6)
                    #
                    #     ax[0].set_title('Magnitude')
                    #     ax[0].legend([f'Mean: {frame_data["wmag_entropy"]}'])
                    #     ax[1].set_title('Orientation')
                    #     ax[1].legend([f'Mean: {frame_data["wang_entropy"]}'])
                    #     plt.show()
                    #     sleep(1)

                    # sp_mag = cv2.normalize(sp_mag, None, 255, 0, cv2.NORM_MINMAX, cv2.CV_8UC1)
                    # sp_ang = cv2.normalize(sp_ang, None, 255, 0, cv2.NORM_MINMAX, cv2.CV_8UC1)
                    # concatenated_roi = np.hstack((
                    #     roi,
                    #     cv2.cvtColor(sp_mag, cv2.COLOR_GRAY2RGB),
                    #     cv2.cvtColor(sp_ang, cv2.COLOR_GRAY2RGB)
                    # ))
                    # cv2.imshow('superpixels', concatenated_roi)
                    # vid_writer_roi.write(concatenated_roi)

                    vid_writer.write(rgb)

                    # Update first frame
                    first_frame = next_frame
                # Break if number of frames completed
                if current_frame >= n_frames:
                    print(f'Finished video... {current_frame} / {n_frames}')
                    break

                # Break if q pressed
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

            cap.release()
            vid_writer.release()
            # vid_writer_roi.release()  # Superpixels
            vid_writer_visual.release()
            cv2.destroyAllWindows()

        # Save DataFrame
        eye_data = eye_data.rename_axis('frame')

        # Assign eyes names
        eye_data['eye_name'] = ''
        eye_0 = eye_data['eye'] == 0
        eye_1 = eye_data['eye'] == 1
        eye_0_x = eye_data[eye_0]['x'].mean()
        eye_1_x = eye_data[eye_1]['x'].mean()

        if eye_0_x < eye_1_x:
            eye_data.loc[eye_0, 'eye_name'] = 'right'
            eye_data.loc[eye_1, 'eye_name'] = 'left'
        else:
            eye_data.loc[eye_0, 'eye_name'] = 'left'
            eye_data.loc[eye_1, 'eye_name'] = 'right'
        eye_data.to_csv(eye_data_file)
        print(f'Time series saved to: {eye_data_file}')
