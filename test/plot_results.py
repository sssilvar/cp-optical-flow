#!/bin/env python3
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

sns.set()
# sns.set_context("talk", font_scale=1.4)

if __name__ == '__main__':
    # Load data file
    # Test set 1
    # data_file = '/home/ssilvari/Documents/temp/Videos/cp_test/output/optical_flow/GOPR0335_of_eye_1_series.csv'
    # data_file = '/home/ssilvari/Documents/temp/Videos/control_test/output/optical_flow/GOPR0229_of_eye_1_series.csv'

    # Test set 2
    # data_file = '/home/ssilvari/Documents/temp/Videos/cp_2/output/optical_flow/GOPR0258_of_eye_1_series.csv'
    # data_file = '/home/ssilvari/Documents/temp/Videos/control_2/output/optical_flow/GOPR0216_of_eye_1_series.csv'

    # Test set 2 (SACCAD)
    # data_file = '/home/ssilvari/Documents/temp/Videos/control_2/SACCAD/output/optical_flow/GOPR0217_of_eye_1_series.csv'
    # data_file = '/home/ssilvari/Documents/temp/Videos/cp_2/SACCAD/output/optical_flow/GOPR0259_of_eye_1_series.csv'

    # Test Set 3 (SMOOTH)
    # data_file = '/home/ssilvari/Documents/temp/Videos/control_3/SMOOTH/output/optical_flow/GOPR0357_of_eye_1_series.csv'
    # data_file = '/home/ssilvari/Documents/temp/Videos/cp_3/SMOOTH/output/optical_flow/GOPR0306_of_eye_1_series.csv'

    # Updated feature extraction
    # data_file = '/disk/Datasets/RIIE_processed/028/SMOOTH/optical_flow/GOPR0357_features_series.csv'
    data_file = '/disk/Datasets/RIIE_processed/014/SMOOTH/optical_flow/GOPR0306_features_series.csv'

    df = pd.read_csv(data_file)
    print(df.head())

    features = df.columns.tolist()
    features.remove('frame')
    features.remove('eye')
    print(features)

    # for feature in features:
    #     # Plot data
    #     g = sns.FacetGrid(df, col='eye', height=5)
    #     g = g.map(plt.plot, 'frame', feature)

    # df['mu_sig'] = df['ang_th_median'].divide(df['ang_th_std'])
    # area_l, area_r = df.query('eye == 0')['area_roi'].mean(), df.query('eye == 1')['area_roi']
    # print(area_l, area_r)

    # sns.relplot(data=df, x='frame', y='data_new', col='eye', kind='line')
    sns.relplot(data=df, x='frame', y='wang_th_mean', col='eye', kind='line')
    # sns.relplot(data=df, x='frame', y='wy_mean', col='eye', kind='line')

    # sns.relplot(
    #     data=df,
    #     x='wmag_mean',
    #     y='wang_mean',
    #     col='eye',
    #     kind='scatter',
    #     size='frame',
    #     hue='frame',
    #     aspect=1.5
    # )
    # g = sns.FacetGrid(df, col='eye', height=5)
    # g = g.map(plt.plot, 'frame', 'mag_th_std')# .set(ylim=(0,10.0))
    # g = g.map(plt.plot, 'frame', 'ang_mean')# .set(ylim=(0, 0.4))
    plt.show()
